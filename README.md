# Pages web du Réseau Bactériophage France
Ce site web est hébergé sur framagit.org, il utilise le logiciel de gestion de version **GIT** qui permet de gérer différentes versions d'un ensemble de documents. Il permet de faire évoluer un programme en conservant son historique complet et détaillé. Il permet d’ouvrir des tickets correspondant aux évolutions et mises à jour requises qui agissent comme des éléments de documentation des évolutions du logiciel. 

Ce site est généré automatiquement par le générateur de site statique **Hugo**, il est recréé en quelques secondes après chaque modification validée par un **commit** sur la branche principale (**master**). Les articles sont rédigés selon la syntaxe **Org-Mode**. Le fichier [https://framagit.org/phagesfr/sitephagesfr/-/blob/master/SYNTAX.org](SYNTAX.org) répertorie les éléments principaux. 

Une méthode de gestion du site est ici proposée en se focalisant sur la simplicité. Pour un approfondissement des possibilités de ces outils, il faut se référer aux documentations officielles. 

[Le livre Pro GIT (en français)](https://git-scm.com/book/fr/v2)
[Documentation de GIT (en anglais)](https://git-scm.com/doc)

## Gérer les articles
Les articles sont accessibles dans le dossier **Content**, puis éventuellement dans le sous dossier correspondant à sa catégorie. 

Exemple : *content/decouvrir/historique.org* pour l'article Historique de la partie Découvrir du site.

### Modifier, corriger ou compléter un article

#### depuis le site gitlab 
Naviguer jusqu'au fichier correspondant à l'article que vous souhaitez modifier. Sélectionner Edit puis modifier le contenu dans la fenêtre de l'éditeur. Pour valider les changements, rentrez un message décrivant les modifications apportées dans le champ **commit message** prévu à cet effet, puis validez avec le bouton **Commit**.

#### à partir d'une copie locale

##### Utiliser l'application GitHub Desktop
**File** **Clone repository** onglet **URL** 

**Clone** **Clone with https** https://framagit.org/phagesfr/sitephagesfr.git

#### Installer Ungit Ubuntu 18
- Ouvrir terminal
- curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
- fermer et reouvrir terminal
- nvm install node
- nvm install --lts
- nvm install 10.16.3
- nvm use 10.16.3
- npm install -g ungit
- ungit




# Concernant Hugo Pages


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

You'll need to configure your site too: change this line
in your `config.toml`, from `"https://pages.gitlab.io/hugo/"` to `baseurl = "https://namespace.gitlab.io"`.
Proceed equally if you are using a [custom domain][post]: `baseurl = "http(s)://example.com"`.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
