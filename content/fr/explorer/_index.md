* Bienvenue sur le réseau **Bactériophage France**

Le réseau **Bactériophage France** a pour but de promouvoir, coordonner et intégrer les études des interactions bactériophages-bactéries à travers différentes disciplines scientifiques, tout en favorisant l’établissement de collaborations et synergies entre laboratoires.