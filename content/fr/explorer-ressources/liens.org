#+title: Liens (autres groupes)
#+description: 
#+date: 2023-01-08
#+tags[]: 
#+draft: false
#+author: Rémy Froissart

* Liens vers d'autres réseaux de recherches sur les bactériophages

FRANCE : [[/explorer-ressources/giph/][Groupe Interdisciplinaire Phagothérapie (GiPh)]]

-----

#+CAPTION: légende
#+NAME: logo of the Belgian Society for Viruses of Microbes 
#+ATTR_HTML: :align left 
#+ATTR_HTML: :width 80px
[[/images/ressources/BsVoM.png]]

BELGIQUE [[https://www.bsvom.be/en][BSVoM - Belgian Society for Viruses of Microbes]]

-----

#+CAPTION: légende
#+NAME: logo of the German Society for phage studies 
#+ATTR_HTML: :align left  
#+ATTR_HTML: :width 80px
[[/images/ressources/NFP_German.png]]

ALLEMAGNE [[https://nationales-forum-phagen.uni-hohenheim.de/en/phage-research-in-germany][Nationalen Forum Phagen]]

-----
#+CAPTION: légende
#+NAME: logo of the swiss network 
#+ATTR_HTML: :align left  
#+ATTR_HTML: :width 80px
[[/images/ressources/phageSuisse.png]]

SUISSE [[http://phagesuisse.ch/ ][phageSuisse]] : L’association à but non lucratif phageSuisse s’engage pour l’information et la formation des professionnels de la santé à propos du traitement à base de bactériophages.

-----

ESPAGNE [[https://www.redfagoma.es/ ][FAGOMA]] : Le réseau espagnol de bactériophages et d’éléments transducteurs 

-----

#+CAPTION: légende
#+NAME: logo of the canadian network for phage studies
#+ATTR_HTML: :align left  
#+ATTR_HTML: :width 80px
[[/images/ressources/PhageCanada.png]]

CANADA [[https://www.fourwav.es/view/2142/info/][Phage Canada]] : Les symposiums virtuels 2020 de Phage Canada ont pour mission de rassembler les chercheurs canadiens à travers le pays et promouvoir les échanges d'idées dans le domaine de la recherche sur les bactériophages. Le but de ces symposiums est d'offrir une plateforme pour les étudiants et les chercheurs postdoctoraux afin de présenter les résultats de leur recherche et de favoriser les interactions au sein de notre communauté académique, industrielle et gouvernementale grandissante. 

-----
#+CAPTION: légende
#+NAME: logo of the african phage forum
#+ATTR_HTML: :align left  
#+ATTR_HTML: :width 80px
[[/images/ressources/AfricanPhageForum.jpeg]]

AFRIQUE [[https://apf.phage.directory/][Africa Phage forum]] : A Collaborative Network of African phage researchers to promote and sensitize phage research in Africa. Vision: To improve human lives through phage research

* Liens vers d'autres groupements de recherches 

#+CAPTION: légende
#+NAME: logo of the french virological society - société française de virologie
#+ATTR_HTML: :align left  
#+ATTR_HTML: :width 80px
[[/images/ressources/SFV_ logoShort.png]]

  La [[https://sfv-virologie.org/reseau-sfv][Société Française de Virologie (SFV)]] compte un réseau de plus de 1000 virologistes des différents laboratoires de France et d’ailleurs. Elle représente toutes les composantes de la virologie : de la recherche fondamentale à la virologie appliquée et la virologie clinique.
-----

#+CAPTION: légende
#+NAME: logo of the International Society for Viruses of Microorganisms
#+ATTR_HTML: :align left  
#+ATTR_HTML: :width 80px
[[/images/ressources/isvm.jpg]]

  La [[https://isvm.org/][Société internationale des virus de micro-organismes]] (International Society for Viruses of Microorganisms, ISVM) se consacre à l'avancement de la science et de l'utilité des virus des micro-organismes (aussi appelés VoMs) - virus des archées, bactériophages (ou phages, virus de bactéries), virus des eucaryotes microbiens.

-----

#+CAPTION: légende
#+NAME: logo of Phage Directory
#+ATTR_HTML: :align left  
#+ATTR_HTML: :width 80px
[[/images/ressources/PhageDirectory.png]]

  [[https://phage.directory/community][Phage Directory]] : Le "community board" est un lieu où les chercheurs et les passionnés de phages peuvent partager des nouvelles et chercher des conseils, des collaborations, des opportunités, etc. Chaque semaine, Phage Directory met en avant les meilleurs messages de la communauté sur leur bulletin d'information, "Capsid & Tail".

* Liens vers des cours
- [[https://www.enseignement.biologie.ens.fr/spip.php?article246&lang=fr][Qlife Quantitative Biology Winter School : Quantitative Viral Dynamics Across Scales]]
