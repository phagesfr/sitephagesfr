+++
include_toc = true
+++

# Ressources du réseau

Vous trouverez dans cet espace de notre site web, les différentes ressources du réseau "Bactériophages France" concernant :
- les différents [groupes de travail](/explorer-ressources/groupes) et comment s'inscrire sur la [mailing-liste](/explorer-contact) de chaque groupe de travail ainsi que les comptes-rendus des ateliers qui se sont tenus par le passé (thérapie phagique, écologie, génomique, structure, etc);
- les [colloques antérieurs](/explorer-ressources/colloques) et leurs programmes
- les [conférences](/explorer-ressources/conf) données par les membres du réseau
- des [liens](/explorer-ressources/liens) vers d'autres groupements de recherches étrangers et autres informations
- des [liens](https://lite.framacalc.org/qp9t8vzc0e-a6hl) vers le recensement des plateformes utiles et expertes dans l'analyse d'échantillons de phages réalisés au sein du réseau Phages.fr
- la composition du [bureau](/explorer-ressources/bureau) du réseau "Phages.fr" au fil des années 

Pour voir apparaître votre groupe dans les équipes référencées dans notre réseau et sur ce site web, vous pouvez remplir une [fiche](/explorer-ressources/fiche_equipe) et l'envoyer (accompagnée d'une image décrivant vos activités en format .jpg) au [contact du site web](mailto:contact@phages.fr)
