#+title: Groupes de travail thématiques
#+description: 
#+date: 2024-02-30
#+tags[]: 
#+draft: false
#+author: Rémy Froissart

En plus du colloque annuel ([[mailto:phages.fr-subscribe@services.cnrs.fr][courriel pour s'inscrire à la mailing-liste générale]]), les "*groupes de travail thématiques*" se réunissent lors de réunions spécifiques regroupant un nombre limité de membres du réseau. Il s’agit de réunions de travail ayant pour but de dynamiser les échanges entre personnes concernées par le thème abordé mais également d’assurer la formation à d’autres disciplines. 

Pour vous inscrire sur les listes mails de la liste générale et celles des groupes thématiques, suivez la procédure expliquée [[/explorer-contact/ ][ici]]

Les thèmes des groupes thématiques et ateliers peuvent être librement/spontanément proposés au bureau ([[mailto:phages.fr-bureau-rtp@services.cnrs.fr][courriel]]) par chaque membre du réseau Phages.fr. Une courte justification de l’intérêt de l’atelier (10 lignes max) et une estimation du budget nécessaire sont requises pour l'évaluation par le bureau. A l’issue de chaque atelier un compte-rendu est rédigé et partagé au sein du réseau (voir ci-dessous).

* Thérapie phagique
Coordination principale : Laurent Debarbieux (laurent.debarbieux@pasteur.fr) & Rémy Froissart (remy.froissart@cnrs.fr)
Pour s'inscrire à la mailing-liste thématique, cliquer [[mailto:phages.fr-therapie-subscribe@services.cnrs.fr][ici]]

- 2023 : Naissance d’un groupe transdisciplinaire français dédié à la phagothérapie ! [[https://www.jle.com/fr/revues/vir/e-docs/naissance_dun_groupe_transdisciplinaire_francais_dedie_a_la_phagotherapie__332694/article.phtml][voir l'annonce sur le site de Virologie]] ou téléchargez le [[/images/ressources/230702 Editorial phagotherapie-Virologie.pdf ][texte non-formaté ici]]

- 2021 : atelier reporté pour cause COVID.  Coordination : Charlotte Brives & Alexandre Bleibtreu & Rémy Froissart. 

- 2019 (Atelier du 3 décembre) réalisé aux Hospices Civiles de Lyon. Coordination : Rémy Froissart. 

- 2017 (Atelier du 12-13 octobre ) à l’Institut Pasteur. Coordination : Laurent Debarbieux & Rémy Froissart : télécharger les compte-rendus des journées du 12 octobre ([[/images/ressources/2017_Workshop_Phagotherapie_AspectsScientifiques.pdf][aspects scientifiques]]) et du 13 octobre ([[/images/ressources/2017_Workshop_Phagotherapie_AspectsMedicaux.pdf][aspects médicaux]])


* Écologie des phages
Coordination principale : Xavier Bellanger (xavier.bellanger@univ-lorraine.fr)
Pour s'inscrire à la mailing-liste thématique, cliquer [[mailto:phages.fr-ecology-subscribe@services.cnrs.fr][ici]]

- Visio-conférence le 2 Octobre 2020. Coordination : Xavier Bellanger & Clara Torres-Barceló. Voir le [[/images/ressources/2020_Programme-Ecologie.pdf][programme]]

- Réunion du 27 mars 2019 à Lyon : Coordination : Xavier Bellanger & Mai Huong Chatain. Télécharger le [[/images/ressources/CR_Ecologie_2019-03-27.pdf][compte-rendu]]

- Réunion du 15 mai 2018 à Nancy : Coordination : Xavier Bellanger & Christophe Ganzer & Hélène  télécharger le [[/images/ressources/CR_Ecologie_2018-05-15.pdf][compte-rendu]]

- Réunion du 20 novembre 2017 à Gif-sur-Yvette : télécharger le [[/images/ressources/CR_Ecologie-2017_11_20.pdf][compte-rendu]]

* Génomique et computational biology
Coordination principale : Marie-Agnès Petit (marie-agnes.petit@inrae.fr)
Pour s'inscrire à la mailing-liste thématique, cliquer [[mailto:phages.fr-computbiol-subscribe@services.cnrs.fr][ici]]

- Atelier du 4 juin 2021 (visio) : "PHROGATON" (présentation de la base de donnée PHROGS). Voir le [[/images/ressources/2021_CR_workshop_comput_biol_PHROGATON.pdf][compte-rendu]]. Cette base de données est dédiée aux virus des bactéries et des archeae. Basée sur la création de groupes de protéines virales orthologues (les PHROGs), elle vise à fournir aux annotateurs de génomes viraux une annotation standardisée.
- Atelier du 9-10 Octobre 2019 : “phage genome sequencing and annotation”. Voir le [[/images/ressources/2019_Workshop_Sequençage_Annotation_9-10_octobre_2019.pdf][compte-rendu]].
- “Viromathon”, LIRMM, Montpellier , 18 octobre 2018 : télécharger le [[/images/ressources/2018-Compte-rendu-Viromathon.pdf][Compte rendu Viromathon]]
- Atelier du 8 septembre 2017 à Clermont Ferrand : télécharger le [[/images/ressources/2017_worshop-Comput-Biol-Phages.fr-Clermont-Ferrand.pdf][compte-rendu]]

* Mécanismes moléculaires de l'infection
Coordination principale : Cécile Breyton (cecile.breyton@ibs.fr) et François Lecointe (francois.lecointe@inrae.fr)
Pour s'inscrire à la mailing-liste thématique, cliquer [[mailto:phages.fr-molec-subscribe@services.cnrs.fr][ici]]

- 12-13 Sept. 2018 : Atelier “Utilisation de la Microscopie Électronique pour l’étude des bactériophages” à l’Institut de Biologie Structurale, Grenoble. Voir le [[/images/ressources/2018_Atelier-Microscopie.pdf][compte-rendu]]
- 29-30 Novembre 2023 : "Prédiction de structure/fonction de protéine via la bio-informatique et l’intelligence artificielle, notamment Alphafold" à l’Institut de Biologie Structurale, Grenoble. Voir le [[/images/ressources/231130_Bilan_Atelier_GT_structure_Alfafold.pdf][compte-rendu]]. 

* ApprentiPhages.fr
Coordination principale :  [[mailto:ombeline.rossier@universite-paris-saclay.fr][Ombeline Rossier]]

Objectifs : voir la [[/explorer-ressources/apprentiphages][page dédiée]]

* Webinars
Coordination principale : [[mailto:anne.chevallereau@cnrs.fr][Anne Chevallereau]]

Objectifs : voir la [[/explorer-ressources/webinars][page dédiée]]

-----

* Atelier "génétique des phages"
** 1er Juin 2023 : CEFE, 1919 route de Mende, Montpellier
Organisé par [[mailto:frederique.le-roux@sb-roscoff.fr][Fréderic Le Roux]] et [[mailto:sylvain.gandon@cefe.cnrs.fr][Sylvain GANDON]]

Program (talks in english or french but slides in english)	
|------------------------------------+-----------------------------|
|Time |	Title |	Speaker |
| 9h30-10h | Introduction | David Bikard (Pasteur, Paris) |
| 10h-10h30 | Engineering T7/HK620 |Juliàn Bulssico (LCB, Marseille) |
| 10h30-11h | Dilution amplification screening in T4 | Marie Agnes Petit (MICALIS, Jouy) |
| 11h-11h30 | Vibriophage mutagenesis | Damien Piel (IBMM, Roscoff) |
| 11h30-12H | T5 mutagenesis | Amandine Maurin (MIVEGEC, Montpellier) |
| 12h-12h30 | T4 with CRISPR-cas tools | Yuvaraj  Bhoobalan (MICALIS, Jouy) |
| 12h30-14h30 | Lunch	 |
| 14h30-18h | Discussion, exchange protocols etc. |
|------------------------------------+-----------------------------|

