+++
include_toc = true
+++


# Les applications antibactériennes des bactériophages

Vous trouverez dans cet espace de notre site web la présentation de l'utilisation des bactériophages pour contrôler les infections bactériennes humaines, agricoles (santé des animaux domestiques, santé des plantes) et industrielles (Biocontrôle de l'eau potable, Biocontrôle des dispositifs). 

Pour toute demande médicale, merci de contacter [PHAGEinLYON_clinic](https://www.crioac-lyon.fr/phagotherapie-bacteriophage/) et plus particulièrement la [fiche contact](https://www.crioac-lyon.fr/contact/)

## Résumé
Dès 1917, dans l’article où il décrit ses premières observations et propose le nom de bactériophage, Félix d’Hérelle rapporte une première utilisation de ces virus pour traiter des infections bactériennes, donnant ainsi naissance à la phagothérapie. Le développement de cette application a montré qu’il était possible de mettre en place des traitements ciblant spécifiquement les bactéries, tels que ceux basés sur l’utilisation d’antibiotiques. Ces derniers, de par leur très grande efficacité et simplicité d’utilisation, ont provoqué le quasi abandon de la phagothérapie. Aujourd’hui, un nombre croissant de bactéries pathogènes pour l’humain présente des phénotypes de résistance à plusieurs familles d’antibiotiques. Les infections causées par ces bactéries imposent l’utilisation de molécules au spectre d’activité toujours plus large et placent certains patients en situation d’impasse thérapeutique. Cet état de fait a relancé les recherches pour permettre le déploiement de la phagothérapie afin de traiter des patients mais aussi des animaux et des plantes. En fait, les domaines d’applications thérapeutiques des bactériophages s’élargissent au fur et à mesure que les agents antibactériens de nature chimique sont remis en cause, voire interdits. Cette revue revient sur les principes fondamentaux des applications thérapeutiques des bactériophages et expose les données récentes dans les domaines pour lesquels une exploitation commerciale est en cours ou sur le point d’émerger.
