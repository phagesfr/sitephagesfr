+++
include_toc = true
+++

# Un siècle de recherche sur les bactériophages

Vous trouverez dans cet espace de notre site web la présentation de l'[historique](/decouvrir-historique/historique) concernant la découverte et l'utilisation des bactériophages en tant qu'outils scientifiques dans la découverte du vivant. 

## Résumé

Les bactériophages tiennent une place prépondérante dans le monde vivant. Du point de vue épistémologique, ils participent à la compréhension du vivant via trois aspects fondamentaux : 
1. les mécanismes moléculaires les plus intimes du fonctionnement des infections virales et de celui des cellules elles-mêmes (biologie moléculaire); 
2. le fonctionnement des écosystèmes (écologie), et 
3. la dynamique adaptative des populations virales et bactériennes (évolution). 

A travers une présentation historique de la découverte des bactériophages et de leurs fascinantes propriétés, cette revue relate la genèse des découvertes fondamentales associées à l’étude des bactériophages et présente une vision synthétique des champs de recherche actuels.
