
#+title: Institut de Biologie Structurale (UMR5075, Grenoble)
#+description: Équipe C. Breyton et G. Schoehn, CNRS, Grenoble - Institut de Biologie Structurale, UMR5075 CNRS - CEA - Univ. Grenoble Alpes
#+date: 2020-11-18
#+tags[]: Interactions-cellulaires-et-moléculaires
#+draft: false
#+author: Cécile Breyton
#+Type: explorerequipes
-----
#+CAPTION: légende
#+NAME: Présentation graphique et photo de l'équipe
#+ATTR_HTML: :alt Représentation de la queue d'un phage, perforation, cryo-EM, tomography, cristallography, NMR, jumbo phages
#+ATTR_HTML: :width 800px
[[/images/equipes/Breyton-Schoehn.jpeg]]
-----
*** Équipe dirigée par Cécile Breyton (CNRS, Grenoble), Structure et Stabilité de Protéines Membranaires Intégrales et Assemblages de Phages

[[https://www.ibs.fr/recherche/groupes-de-recherche/groupe-membrane-et-pathogenes-f-fieschi/ssimpa/article/structure-et-stabilite-de-proteines-membranaires-integrales-et-assemblages-de?lang=fr ][site du laboratoire]]

*Composition de l'équipe* : 8 personnes dont 6 permanents 

*Sujet principal* : Nous nous intéressons aux mécanismes moléculaires de la perforation de la paroi de E. coli par le phage T5, mais également à la structure de Jumbo phages, incluant la capside et la queue.

*Mots-clés* : Biologie structurale, Interaction phage-hôte, Cryo-microscopie électronique

*Interactions*
|------------------------------------+-----------------------------|
| *Nom de l'espèce bactérienne ciblée* | *Nom du/des bactériophage(s)* |
|/Escherichia coli/ | T5 |
|  | Jumbo phages RSL1, RSL2, XacN1, RP13 et Esc1  |
|------------------------------------+-----------------------------|
#+TBLFM: $4=$3/$2;%.1f

