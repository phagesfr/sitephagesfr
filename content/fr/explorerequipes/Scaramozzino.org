#+title: Plateforme M2Bio (LIPhy, Grenoble)
#+description: Équipe N. Scaramozzino, CNRS, Saint Martin d'Hères, Laboratoire Interdisciplinaire de PHYsique UMR5588 Université Grenoble Alpes / CNRS
#+date: 2020-10-28
#+tags[]: Interactions-cellulaires-et-moléculaires Thérapie-Biocontrôle 
#+draft: false
#+author: Scaramozzino
#+Type: explorerequipes

-----
#+CAPTION: légende
#+NAME: Présentation graphique et photo de Natale Scaramozzino
#+ATTR_HTML: :alt Représentation schématique de la méthode phage display [décrire la photo]
#+ATTR_HTML: :width 800px
[[/images/equipes/Scaramozzino.jpg]]
-----
*** Natale Scaramozzino ([[mailto:natale.scaramozzino@univ-grenoble-alpes.fr][courriel]]), CNRS, Saint Martin d'Hères - Laboratoire Interdisciplianire de PHYsique UMR5588 Université Grenoble Alpes / CNRS 

[[https://www-liphy.univ-grenoble-alpes.fr/M2Bio][site du laboratoire]]

*Composition de la plateforme* : 3 personnes dont 2 permanents

*Sujet principal* : Nous sommes des microbiologistes utilisant la méthode "phage display" afin de sélectionner des fragments d'anticorps (ScFv, VH...) ou des peptides vis-à-vis de cibles innovantes comme de l'ADN avec des topologies particulières (collaboration Eric Defrancq, DCM à Grenoble) ou de composés organiques volatils (collaboration Yanxia HOU et Arnaud BUHOT, SyMMES à Grenoble).

*Mots-clés* : Phage display, présentation d'anticorps ou de peptides, banque anticorps, bio-détection

*Interactions*
|-----------------------------------+-----------------------------|
| *Nom de l'espèce bactérienne cible* | *Nom du/des bactériophage(s)* |
|/Escherichia coli/ (TG1) | M13 
|-----------------------------------+-----------------------------|
#+TBLFM: $4=$3/$2;%.1f



