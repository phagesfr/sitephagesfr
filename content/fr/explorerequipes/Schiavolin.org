#+title: Interactions Phage - Streptococcus pyogenes - hôte (BacMol, Bruxelles)
#+description: Équipe Lionel Schiavolin, post-doc FNRS, Bruxelles, Laboratoire de Bactériologie Moléculaire BacMol - Université Libre de Bruxelles (ULB)
#+date: 2020-10-29
#+tags[]: Interactions-cellulaires-et-moléculaires
#+draft: false
#+author: Lionel Schiavolin
#+Type: explorerequipes

-----
#+CAPTION: légende
#+NAME: Présentation graphique et photo de l'équipe dirigée par Lionel Schiavolin
#+ATTR_HTML: :alt Représentation d'une bactérie infectée par un phage et voies intracellulaires, du système respiratoire supérieur et descriptif équipe
#+ATTR_HTML: :width 800px
[[/images/equipes/Schiavolin.png]]
-----
*** Lionel Schiavolin ([[mailto:lionel.schiavolin@ulb.be][courriel]]), post-doc FNRS, Laboratoire de Batériologie Moléculaire - Université Libre de Bruxelles (ULB)

[[http://cvchercheurs.ulb.ac.be/Site/unite/ULB588.php][site du laboratoire]]

*Composition de l'équipe* : 3 personnes dont 1 permanent

*Sujet principal* : L'équipe phage fait partie du Laboratoire de Bactériologie Moléculaire, dirigé par Anne Botteaux et Pierre Smeesters, constitué de microbiologistes moléculaires et de médecin-chercheurs qui s'intéressent à la virulence de Streptococcus pyogenes, au développement d'un vaccin, ainsi que d'outils de détection pour l'otite moyenne aiguë. La thématique que je développe s'intéresse aux interactions entre S. pyogenes et les phages virulents/tempérés qui l'infectent. D'une part, nous essayons de comprendre l'impact d'une infection par un phage virulent sur le programme transcriptionnel et la synthèse protéique de S. pyogenes avec un focus particulier sur son métabolisme et ses interactions avec la flore commensale présente au niveau de l'hôte humain. D'autre part, nous essayons de comprendre les avantages conférés par les phages tempérés sur la résistance aux phages et sur la virulence de S. pyogenes. Ces approches devraient améliorer nos connaissances des interactions entre phages et bactéries pathogènes dans le but d'identifier de nouvelles cibles thérapeutiques et de mieux décrire les mécanismes moléculaires associés à la phagothérapie.

*Interactions*
|-----------------------------------+-----------------------------|
| *Nom de l'espèce bactérienne cible* | *Nom du/des bactériophage(s)* |
| /Streptococcus pyogenes/ | phage A25 & prophages 		  |
|-----------------------------------+-----------------------------|
#+TBLFM: $4=$3/$2;%.1f

*Mots-clés* : transcriptomique, protéomique, phages virulents, prophages, interactions hôte-pathogène
