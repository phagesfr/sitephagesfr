#+title: Microbiote, Intestin et Inflammation (UMRS 938 Centre de Recherche Saint Antoine, Paris)
#+description: Groupe Phages, Intestin et Inflammation, Luisa De Sordi, Equipe Microbiote, Intestin et Inflammation, Philippe Seksik et Harry Sokol, Centre de Recherche Saint Antoine, Paris, Sorbonne Université, INSERM
#+date: 2020-12-17 
#+tags[]: Écologie-Évolution Interactions-cellulaires-et-moléculaires Thérapie-Biocontrôle 
#+draft: false
#+author: Luisa De Sordi
#+Type: explorerequipes

-----
#+CAPTION: légende
#+NAME: Présentation graphique et photo du groupe de Luisa De Sordi
#+ATTR_HTML: :alt Représentation thématiques (inflammatory bowel disease, faecal transplant, infection)
#+ATTR_HTML: :width 800px
[[/images/equipes/de_sordi.png]]
-----
*** Luisa De Sordi ([[mailto:luisa.de_sordi@sorbonne-universite.fr][courriel]]), Centre de Recherche Saint Antoine, Paris - UMRS 938 Paris 
[[https://www.crsa.fr/equipe-philippe-seksik.html ][site du laboratoire]]

*Composition de l'équipe* : 8 personnes dont 3 permanents 

*Sujet principal* : Le groupe "Phages, Intestin et Inflammation" fait partie de l’équipe "Microbiote, Intestin et Inflammation" dirigé par Philippe Seksik et Harry Sokol. Nous nous intéressons aux phages intestinaux et à leurs interactions avec les bactéries du microbiote et la barrière intestinale dans un contexte inflammatoire. Notre recherche comprend l’étude de modèles cellulaires in vitro, de modèles murins et d’échantillons cliniques issus de patients atteints de maladies inflammatoires chroniques de l’intestin (MICI). 

*Mots-clés* : Microbiote, ecologie, génomique, viromes, barrière intestinale, Maladies Inflammatoires Chroniques de l'Intestin (MICI), transplantation fécale

