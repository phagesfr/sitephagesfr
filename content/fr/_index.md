+++
  include_toc = true
+++

# Bienvenue sur le site du réseau "Phages.fr"

Le réseau "Phages.fr" regroupe des scientifiques qui travaillent sur les thématiques incluant les bactériophages dont l'étude des structures, interactions moléculaires & cellulaires, écologie & évolution, thérapie phagique et biocontrôle en santé humaine, végétale & animale et industrie ainsi que des Sciences humaines et sociales. Ces scientifiques appartiennent à différents instituts publics français de recherche tels que CNRS (principal financeur), INRAE, INSERM, ANSES, Institut Pasteur mais aussi de nombreuses universités (Bordeaux, Lyon, Marseille, Montpellier, Nancy, Paris, etc) et hôpitaux, avec aussi la participation de membres d'entreprises. Ce site web cherche donc à réunir et présenter au mieux les recherches actuelles menées par les équipes françaises. [N'hésitez pas à nous rejoindre !](/explorer-contact) 

-----

# Actualités

### Webinars 
Le prochain webinar (voir la [page dédiée ici](/explorer-ressources/webinars/)) sera donné le mercredi 26 Mars 2025 à 13h30 par Julien Lopez, doctorant dans le groupe de Marianne DE Paepe au sein de l'UMR MICALIS dans l'équipe MUSE [Mutagenesis in Single-Cells and Evolution](https://www.micalis.fr/equipe/muse/), Jouy en Josas. Vous pouvez visionner les séminaires en distanciel (voir lien) et les replay des présentations précédentes [ici](https://www.canal-u.tv/chaines/phages). 

### Table ronde "Phages & biocontrôle" (Congrès de la Société française de Phytopathologie (SFP) le 23 mai 2025)
Voir l'[annonce](/images/ressources/giph/250523_table_ronde_phage_biocontrol_SFP.jpg) de la table ronde organisée par le [Groupe Interdisciplinaire Phagothérapie (GiPh)](/explorer-ressources/giph/). 

### Colloque annuel du réseau : 12-14 Nov. 2025 à Nancy
Le prochain colloque du réseau "Phages.fr" aura lieu du 12 au 14 Novembre à Nancy. Voir les informations sur la [page dédiée aux colloques annuels](/explorer-ressources/colloques/)
