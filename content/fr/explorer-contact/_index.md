# Contact

Pour toute question ou modification du site web, merci d’adresser votre demande au [contact du site web](mailto:contact@phages.fr) et/ou à vous référer aux responsables des [groupes thématiques](/explorer-ressources/groupes) pour vous abonner aux différentes [mailing-listes](/explorer-contact). 

Le réseau est animé par un bureau qui peut être contacté à l' [adresse suivante](mailto:phages.fr-bureau-rtp@services.cnrs.fr). 

----

## Mailing-listes

Au sein du réseau Phages.fr, il existe plusieurs groupes de travail et donc plusieurs mailing-listes :

- la liste générale : phages.fr

et les groupes thématiques : 
- phages.fr-ecology
- phages.fr-therapie
- phages.fr-computbiol
- phages.fr-molec
- phages.fr-comm
- apprentiphages

### Inscription à une mailing-liste

Si vous souhaitez intégrer (ou changer d'adresse mail) l’un des groupe, il vous suffit d’envoyer un courriel à : sympa@services.cnrs.fr

avec, dans l’objet de votre message:

- SUBSCRIBE phages.fr-NomDuGroupe VotreAdresseMail , si vous souhaitez vous *inscrire* 
(exemple : subscribe phages.fr-therapie jean.dupond@fai.fr)
- UNSUBSCRIBE phages.fr-NomDuGroupe VotreAdresseMail , si vous souhaitez vous *désinscrire*
- INVITE phages.fr-NomDuGroupe VotreAdresseMail, si vous souhaitez *inviter* quelqu’un à rejoindre ce groupe

### Envoi de courriel à une liste
Une fois votre inscription à la liste réalisée, envoyez votre courriel à l'adresse : NomDuGroupe@services.cnrs.fr

Par exemple, si vous voulez envoyer un courriel à la liste générale, il faut l'envoyer à [phages.fr@services.cnrs.fr](mailto:phages.fr@services.cnrs.fr)


