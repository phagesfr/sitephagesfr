+++
include_toc = true
+++

## Découvrir les bactériophages, la phagothérapie et le Biocontrôle par les bactériophages

Vous trouverez dans cet espace de notre site web la présentation de : 
1. l'[historique](/decouvrir-historique) concernant la découverte et l'utilisation des bactériophages en tant qu'outils scientifiques dans la découverte du vivant ; 
2. l'[application](/decouvrir-applications) des bactériophages pour contrôler les infections bactériennes humaines, agricoles (santé des animaux domestiques, santé des plantes) et industrielles (Biocontrôle de l'eau potable, Biocontrôle des dispositifs). 