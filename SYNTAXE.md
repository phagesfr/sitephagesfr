POUR L'ECRITURE EN MARKDOWN

#+ faire un lien interne 
[colloques antérieurs](/explorer-ressources/colloques.org ) 

#+ image
## Image seule 
![texte descriptif](/images/sitephagesfr_applications.jpg)

## Image avec texte
<img href="https://site.phages.fr/images/ressources/colloques_annuels/2024_affiche_phagesinsete.jpg" class="imageagauche" width="200px" alt="bacteriophage tintaine joute Sète bactérie bacteria"/>

#+ Titres 
# Titre 1
## Titre 2
etc

#+ pour les points numérotés
1. bla bla
2. et encore, blo blo blo
3. et ainsi de suite
ou bien 
- bla bla
- blo blo blo
- blu blu blu blu

